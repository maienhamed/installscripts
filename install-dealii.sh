#!/bin/bash

# Requires: gfortran libblas-dev liblapack-dev libboost-dev
set -e

OPENMPI_PATH=https://download.open-mpi.org/release/open-mpi/v5.0/openmpi-5.0.6.tar.bz2
OPENMPI_MD5=6b49d5fb496749639978f39aeaace116
P4EST_PATH=https://p4est.github.io/release/p4est-2.8.6.tar.gz
CMAKE_PATH=https://github.com/Kitware/CMake/releases/download/v3.31.1/cmake-3.31.1.tar.gz
TRILINOS_PATH=https://github.com/trilinos/Trilinos/archive/refs/tags/trilinos-release-16-0-0.tar.gz
DEAL_II_PATH=https://dealii.org/downloads/dealii-9.6.0.tar.gz
P4EST_SETUP_SCRIPT_PATH=https://www.dealii.org/9.6.0/external-libs/p4est-setup.sh

WORKDIR=$PWD
INSTALLDIR=$HOME/share

# Install openmpi
wget --no-check-certificate --continue $OPENMPI_PATH &&                                \
    export OPENMPI_FILE=$(echo $OPENMPI_PATH | sed "s:.*/\([^/]*\):\1:") &&            \
    #if [[ $OPENMPI_MD5 == $($OPENMPI_FILE | awk '{print $1}') ]]; then exit 1; fi &&  \
    tar -jxf $OPENMPI_FILE &&                                                          \
    #rm $OPENMPI_FILE &&                                                               \
    export OPENMPI_DIR=$(echo $OPENMPI_PATH | sed "s:.*/\([^/]*\).tar.bz2:\1:") &&     \
    cd ${OPENMPI_DIR} &&                                                               \
    ./configure --prefix=$INSTALLDIR CC=cc CXX=g++ F77=gfortran FC=gfortran &&         \
    make -j $(nproc) all && make install &&                                            \
    cd $WORKDIR &&                                                                     \
    rm -rf ${OPENMPI_DIR}

export                              \
    CC=$INSTALLDIR/bin/mpicc        \
    CXX=$INSTALLDIR/bin/mpicxx      \
    F77=$INSTALLDIR/bin/mpif77      \
    FC=$INSTALLDIR/bin/mpif90       \
    FORT=$INSTALLDIR/bin/mpifort

# Install p4est
export P4EST_FILE=$(echo $P4EST_PATH | sed "s:.*/\([^/]*\):\1:") &&                              \
        [[ -d scripts ]] || mkdir scripts &&                                                     \
    wget --no-check-certificate --continue -O scripts/p4est-setup.sh $P4EST_SETUP_SCRIPT_PATH && \
    wget --no-check-certificate --continue -O scripts/$P4EST_FILE $P4EST_PATH &&                 \
    bash scripts/p4est-setup.sh scripts/$P4EST_FILE $INSTALLDIR/p4est &&                         \
    rm scripts/$P4EST_FILE

# install cmake
wget --no-check-certificate --continue $CMAKE_PATH &&                                  \
    export CMAKE_FILE=$(echo $CMAKE_PATH | sed "s:.*/\([^/]*\):\1:") &&                \
    tar -zxf $CMAKE_FILE -C $WORKDIR &&                                                \
    #rm $CMAKE_FILE &&                                                                 \
    export CMAKE_DIR=$(echo $CMAKE_FILE | sed "s:\(.*\).tar.gz:\1:") &&                \
    cd $WORKDIR/$CMAKE_DIR &&                                                          \
    ./configure --prefix=$INSTALLDIR &&                                                \
    make -j $(nproc) &&                                                                \
    make install &&                                                                    \
    cd $WORKDIR &&                                                                     \
    rm -rf $CMAKE_DIR

# install trilinos
wget --no-check-certificate --continue $TRILINOS_PATH &&                               \
    export TRILINOS_FILE=$(echo $TRILINOS_PATH | sed "s:.*/\([^/]*\):\1:") &&          \
    tar -zxf $TRILINOS_FILE -C $WORKDIR &&                                             \
    #rm $TRILINOS_FILE &&                                                              \
    export TRILINOS_DIR=Trilinos-$(echo $TRILINOS_FILE | sed "s:\(.*\).tar.gz:\1:") && \
    mkdir -p $WORKDIR/$TRILINOS_DIR/build &&                                           \
    cd $WORKDIR/$TRILINOS_DIR/build &&                                                 \
    $INSTALLDIR/bin/cmake                                                              \
        -DTrilinos_ENABLE_Amesos=ON                                                    \
        -DTrilinos_ENABLE_Epetra=ON                                                    \
        -DTrilinos_ENABLE_EpetraExt=ON                                                 \
        -DTrilinos_ENABLE_Ifpack=ON                                                    \
        -DTrilinos_ENABLE_AztecOO=ON                                                   \
        -DTrilinos_ENABLE_Sacado=ON                                                    \
        -DTrilinos_ENABLE_SEACAS=ON                                                    \
        -DTrilinos_ENABLE_Teuchos=ON                                                   \
        -DTrilinos_ENABLE_MueLu=ON                                                     \
        -DTrilinos_ENABLE_ML=ON                                                        \
        -DTrilinos_ENABLE_NOX=ON                                                       \
        -DTrilinos_ENABLE_ROL=ON                                                       \
        -DTrilinos_ENABLE_Tpetra=ON                                                    \
        -DTrilinos_ENABLE_COMPLEX=ON                                                   \
        -DTrilinos_ENABLE_FLOAT=ON                                                     \
        -DTrilinos_ENABLE_Zoltan=ON                                                    \
        -DTrilinos_VERBOSE_CONFIGURE=OFF                                               \
        -DTPL_ENABLE_Netcdf=OFF                                                        \
        -DTrilinos_ENABLE_SEACAS=OFF                                                   \
        -DTPL_ENABLE_MPI=ON                                                            \
        -DMPI_BASE_DIR:PATH=$INSTALLDIR                                                \
        -DBUILD_SHARED_LIBS=ON                                                         \
        -DCMAKE_VERBOSE_MAKEFILE=OFF                                                   \
        -DCMAKE_BUILD_TYPE=RELEASE                                                     \
        -DCMAKE_INSTALL_PREFIX:PATH=$INSTALLDIR                                        \
        $WORKDIR/$TRILINOS_DIR/ &&                                                     \
    make -j $(nproc) &&                                                                \
    make install &&                                                                    \
    cd $WORKDIR &&                                                                     \
    rm -rf $WORKDIR/$TRILINOS_DIR

# install dealii
wget --no-check-certificate --continue $DEAL_II_PATH &&                                \
    export DEAL_II_FILE=$(echo $DEAL_II_PATH | sed "s:.*/\([^/]*\):\1:") &&            \
    tar -zxf $DEAL_II_FILE -C $WORKDIR &&                                              \
    #rm $DEAL_II_FILE &&                                                               \
    export DEAL_II_DIR=$(echo $DEAL_II_FILE | sed "s:\(.*\).tar.gz:\1:") &&            \
    [[ -d $WORKDIR/deal-ii-build ]] || mkdir $WORKDIR/deal-ii-build &&                 \
    cd $WORKDIR/deal-ii-build &&                                                       \
    $INSTALLDIR/bin/cmake -DDEAL_II_WITH_TRILINOS=ON                                   \
          -DTRILINOS_DIR=$INSTALLDIR                                                   \
          -DTrilinos_INCLUDE_DIRS=$INSTALLDIR                                          \
          -DDEAL_II_WITH_MPI=ON                                                        \
          -DMPI_DIR=$INSTALLDIR                                                        \
          -DDEAL_II_WITH_P4EST=ON                                                      \
          -DP4EST_DIR=$INSTALLDIR/p4est/FAST                                           \
          -DCMAKE_INSTALL_PREFIX=$INSTALLDIR                                           \
          $WORKDIR/$DEAL_II_DIR/ &&                                                    \
    make -j $(nproc) && make install &&                                                \
    cd $WORKDIR &&                                                                     \
    rm -r $WORKDIR/deal-ii-build &&                                                    \
    rm -r $WORKDIR/$DEAL_II_DIR/
